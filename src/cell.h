/*
A terminal based Sudoku puzzle solver.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef SUDOKU_CELL
#define SUDOKU_CELL

#include "common.h"

struct possibility
{
  unsigned int value;
  struct possibility *next;
};

struct cell
{
  unsigned int value;
  boolean is_set;
  struct cell ***groups;
  unsigned int group_count;

  unsigned int possibility_count;
  struct possibility *possibilities;
};

void init_cell(struct cell *new_cell, unsigned int group_size);
void terminate_cell(struct cell *which);

boolean is_cell_set(struct cell *which);

void set_cell_value(struct cell *which, unsigned int value);

void set_test_value(struct cell *which, unsigned int value);

void unset_cell_value(struct cell *which);

int get_cell_value(struct cell *which);

boolean is_possible(struct cell *which, unsigned int value);

void eliminate_possibility(struct cell *which, unsigned int value);

void
print_possibilites (struct cell *which, int x, int y);

#endif
