/*
A terminal based Sudoku puzzle solver.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef SUDOKU_GRID
#define SUDOKU_GRID

#include "common.h"
#include "cell.h"

extern unsigned int group_size;

/* Have to pass in by reference because since I'm using malloc to allocate
   arrays dynamically if I don't pass by reference the reference address that is assigned will not make it back to the
   calling grid.  If I had passed by value it would have put the new
   address into the local variable (new_grid) without changing the
   address contained in the calling variable (grid).
   If I had not added pass by reference this function would create a new
   grid then the memory allocated would be marooned (allocated but no
   longer accessable) when the function terminates.*/
enum Return_Code
create_grid (struct cell ****new_grid);

enum Return_Code
associate_groups (struct cell ****groups,
                  struct cell ***grid);

enum Return_Code
associate_cells (struct cell ***groups,
                 struct cell ***grid,
                 unsigned int max_groups_per_cell);

void
add_cell_to_group (struct cell ***group,
                   unsigned int group_pointer_index,
                   unsigned int cell_pointer_index,
                   struct cell *** grid,
                   unsigned int x,
                   unsigned int y);

enum Return_Code
delete_grid (struct cell ****terminal_grid,
             struct cell ****terminal_groups,
             unsigned int max_groups_per_cell);

#endif
