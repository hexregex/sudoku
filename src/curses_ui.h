/*
A terminal based Sudoku puzzle solver.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef CURSES_RENDER
#define CURSES_RENDER

#define _XOPEN_SOURCE_EXTENDED

#include <stddef.h>

#ifdef WIN32
#define PDC_WIDE
#include <curses.h>
#endif

#ifdef __CYGWIN__
#include <curses.h>
#endif


#ifdef __GNUC__
#define _XOPEN_SOURCE_EXTENDED
/* #include <ncursesw/curses.h> */
#include <curses.h>
#endif

#include "ui.h"


void curses_init_screen (unsigned int group_size);
void curses_draw_board (unsigned int group_size);
void curses_draw_screen ();
void curses_terminate_board ();
void curses_draw_cell (int x, int y, int value);
void curses_move_cursor (int x, int y);
enum input_code curses_get_keypress ();
void curses_hide_cursor ();
void curses_show_cursor ();

#endif
