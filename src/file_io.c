/*
A terminal based Sudoku puzzle solver.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include <stdio.h>

#include "grid.h"

  /* TODO: Make more robust.  */

struct cell ***
read_grid_from_file (struct cell ****write_grid, const char *file_name, unsigned int group_size)
{
  FILE *input;
  char read;
  int x, y;

  if (*write_grid == NULL)
    create_grid(write_grid);

  /* TODO: Implement exception handling for file not found exception.  */
  input = fopen(file_name, "r");

  for (y = 0; y < group_size; y++)
    {
      for (x = 0; x < group_size; x++)
        {
          set_cell_value ((*write_grid)[x][y], fgetc(input) - '0');
        }

        read = fgetc(input);
    }

  fclose(input);
}
