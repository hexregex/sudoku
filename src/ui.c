/*
A terminal based Sudoku puzzle solver.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "ui.h"
#include "curses_ui.h"


void
init_screen (enum graphical_mode mode, unsigned int group_size)
{
  if (mode == NCURSES)
    {
      init_screen_ptr = &curses_init_screen;
      draw_board_ptr = &curses_draw_board;
      draw_screen_ptr = &curses_draw_screen;
      terminate_board_ptr = &curses_terminate_board;
      draw_cell_ptr = &curses_draw_cell;
      move_cursor_ptr = &curses_move_cursor;
      get_keypress_ptr = &curses_get_keypress;
      hide_cursor_ptr = &curses_hide_cursor;
      show_cursor_ptr = &curses_show_cursor;
    }

  (*init_screen_ptr) (group_size);
}
