/*
A terminal based Sudoku puzzle solver.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "common.h"
#include "cell.h"


/* Assigned in main with pointer indirection.
I want the compiler to error if code attempts to change directly. */
unsigned int group_size;

static unsigned int
extract_group_size (int argc, char *argv[]);

static void
print_grid (struct cell ***);

static enum Return_Code
input_grid (struct cell ****write_grid, unsigned int group_size);

static void
move_cursor_draw_wait (int x, int y);

static enum Return_Code
solve_brute_force_recursive (struct cell ***grid,
                             unsigned int x,
                             unsigned int y);

static enum Return_Code
solve_elimination (struct cell ***grid,
                   struct cell ***groups);

